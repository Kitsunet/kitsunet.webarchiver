<?php
namespace Kitsunet\WebArchiver\Filters;

/**
 * Host filter will allow all urls without a host, or with one of the configured hosts
 *
 */
class HostsFilter extends UrlPartInArrayFilter {

	static protected $URL_PART = 'host';
}
