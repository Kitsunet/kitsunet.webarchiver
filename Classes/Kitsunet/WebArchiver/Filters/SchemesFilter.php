<?php
namespace Kitsunet\WebArchiver\Filters;

/**
 * Schemes filter will allow all urls without a Scheme, or with one of the configured Schemes
 *
 */
class SchemesFilter extends UrlPartInArrayFilter {

	static protected $URL_PART = 'scheme';
}
