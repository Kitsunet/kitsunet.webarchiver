<?php
namespace Kitsunet\WebArchiver\Filters;

/**
 * Class RegexFilter
 *
 */
class RegexFilter implements FilterInterface {

	/**
	 * @var String
	 */
	protected $regex;

	/**
	 * Sets the string that the urls are supposed to contain.
	 *
	 * @param String $regex
	 */
	public function __construct($regex) {
		$this->regex = $regex;
	}

	/**
	 * Filters a url by checking it against a regular expression.
	 *
	 * @param \Guzzle\Http\Url $url
	 *
	 * @return boolean
	 */
	public function filter($url) {
		return (preg_match($this->regex, $url) === 1);
	}
}
