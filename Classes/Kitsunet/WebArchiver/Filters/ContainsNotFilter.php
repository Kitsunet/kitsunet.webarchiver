<?php
namespace Kitsunet\WebArchiver\Filters;

/**
 * Class ContainsNotFilter
 *
 */
class ContainsNotFilter implements FilterInterface {

	/**
	 * @var String
	 */
	protected $string;

	/**
	 * Sets the string that the urls are not supposed to contain.
	 *
	 * @param String $string
	 */
	public function __construct($string) {
		$this->string = $string;
	}

	/**
	 * Filters a url by checking if it doesn't contain the string.
	 *
	 * @param \Guzzle\Http\Url $url
	 *
	 * @return boolean
	 */
	public function filter($url) {
		return (strpos($url, $this->string) === FALSE);
	}
}
