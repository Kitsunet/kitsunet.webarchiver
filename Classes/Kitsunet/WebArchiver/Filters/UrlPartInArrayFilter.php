<?php
namespace Kitsunet\WebArchiver\Filters;

/**
 * Abstract filter for matching a specific part of the URL against an array of strings.
 *
 */
abstract class UrlPartInArrayFilter implements FilterInterface {
	/**
	 * List of host strings to allow for urls.
	 *
	 * @var array
	 */
	protected $allowed = array();

	/**
	 * @var string
	 */
	static protected $URL_PART = '';

	/**
	 * @param array $allowed
	 */
	public function __construct(array $allowed) {
		$this->allowed = $allowed;
	}

	/**
	 * Filters a url by checking if it contains the string.
	 *
	 * @param \Guzzle\Http\Url $url
	 *
	 * @return boolean
	 */
	public function filter($url) {
		$parts = $url->getParts();
		if (isset($parts[static::$URL_PART])) {
			$part = $parts[static::$URL_PART];
			if ($part === NULL || $part === '' || in_array($part, $this->allowed)) {
				return TRUE;
			}
		}

		return FALSE;
	}
}
