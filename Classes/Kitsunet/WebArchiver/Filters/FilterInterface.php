<?php
namespace Kitsunet\WebArchiver\Filters;

/**
 * Interface FilterInterface
 *
 */
interface FilterInterface {

	/**
	 * Check if the given Url object passes the filter.
	 *
	 * @param \Guzzle\Http\Url $url
	 *
	 * @return boolean
	 */
	public function filter($url);
}