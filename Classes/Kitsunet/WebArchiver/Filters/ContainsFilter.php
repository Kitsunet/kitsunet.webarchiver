<?php
namespace Kitsunet\WebArchiver\Filters;

/**
 * Class ContainsFilter
 *
 */
class ContainsFilter implements FilterInterface {

	/**
	 * @var String
	 */
	protected $string;

	/**
	 * Sets the string that the urls are supposed to contain.
	 *
	 * @param String $string
	 */
	public function __construct($string) {
		$this->string = $string;
	}

	/**
	 * Filters a url by checking if it contains the string.
	 *
	 * @param \Guzzle\Http\Url $url
	 *
	 * @return bool
	 */
	public function filter($url) {
		return (strpos($url, $this->string) !== FALSE);
	}
}
