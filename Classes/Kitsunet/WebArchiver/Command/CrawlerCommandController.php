<?php
namespace Kitsunet\WebArchiver\Command;

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Cli\CommandController;
use Kitsunet\WebArchiver\Crawler;

/**
 *
 * @Flow\Scope("singleton")
 */
class CrawlerCommandController extends CommandController {

	/**
	 * @var array
	 */
	protected $settings;

	/**
	 * @Flow\Inject
	 * @var \TYPO3\Flow\Object\ObjectManagerInterface
	 */
	protected $objectManager;

	/**
	 * @param array $settings The WebArchiver settings
	 * @return void
	 */
	public function injectSettings(array $settings) {
		$this->settings = $settings;
	}

	/**
	 * Command to crawl the given URL
	 *
	 * @param string $url Start URL to crawl from.
	 * @return void
	 */
	public function crawlCommand($url) {
		$guzzleClient = new \Guzzle\Http\Client($url);
		$crawler = new Crawler($guzzleClient);
		$this->addFiltersToCrawler($crawler);
		$this->addActionsToCrawler($crawler);
		$this->setTimeouts($crawler);
		$crawler->setMaxDepth($this->settings['depth']);
		if (isset($this->settings['loginRequest']['url'])) {
			$this->doAuthenticationRequest($crawler);
		} elseif (isset($this->settings['loginRequest']['formUrl'])) {
			$this->sendLoginForm($crawler);
		}
		$crawler->addUrl(\Kitsunet\WebArchiver\Http\Url::factory($url));
		$links = $crawler->go();
		foreach ($links as $link) {
			$this->outputLine($link['url']);
		}
	}

	/**
	 * Empty the database of to be crawled and visited urls for the given base url.
	 *
	 * @param string $url
	 */
	public function clearDatabaseCommand($url) {
		$guzzleClient = new \Guzzle\Http\Client($url);
		$crawler = new Crawler($guzzleClient);
		$crawler->addUrl(\Kitsunet\WebArchiver\Http\Url::factory($url));

		$crawler->clearCrawlDatabase();
	}

	/**
	 * @param Crawler $crawler
	 */
	protected function setTimeouts(Crawler $crawler) {
		if (isset($this->settings['crawlerTimeout'])) {
			if (strpos($this->settings['crawlerTimeout'], ',') !== FALSE) {
				list($minTimeout, $maxTimeout) = explode(',', $this->settings['crawlerTimeout']);
				$maxTimeout = intval($maxTimeout);
			} else {
				$minTimeout = $this->settings['crawlerTimeout'];
				$maxTimeout = NULL;
			}
			$minTimeout = intval($minTimeout);
			$crawler->setTimeout($minTimeout, $maxTimeout);
		}
	}

	/**
	 * @param $crawler
	 */
	protected function addFiltersToCrawler(Crawler $crawler) {
		foreach ($this->settings['filters'] as $filterConfiguration) {
			$classAndConstructorArguments = array_merge(array($filterConfiguration['class']), $filterConfiguration['options']);
			$filter = call_user_func_array(array($this->objectManager, 'get'), $classAndConstructorArguments);
			$crawler->addFilter($filter);
		}
	}

	/**
	 * @param Crawler $crawler
	 */
	protected function addActionsToCrawler(Crawler $crawler) {
		foreach ($this->settings['actions'] as $actionConfiguration) {
			$classAndConstructorArguments = array_merge(array($actionConfiguration['class']), $actionConfiguration['options']);
			$action = call_user_func_array(array($this->objectManager, 'get'), $classAndConstructorArguments);
			$crawler->addAction($action);
		}
	}

	protected function doAuthenticationRequest(Crawler $crawler) {
		$crawler->doAuthenticationRequest($this->settings['loginRequest']['url'], $this->settings['loginRequest']['data']);
	}

	protected function sendLoginForm(Crawler $crawler) {
		$crawler->sendLoginForm($this->settings['loginRequest']['formUrl'], $this->settings['loginRequest']['formXPath'], $this->settings['loginRequest']['data']);
	}
}
