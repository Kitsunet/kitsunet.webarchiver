<?php
namespace Kitsunet\WebArchiver;

use Guzzle\Http\Client;
use Guzzle\Plugin\Cookie\CookieJar\FileCookieJar;
use Guzzle\Plugin\Cookie\CookiePlugin;
use Kitsunet\WebArchiver\Http\Url;
use Kitsunet\WebArchiver\Actions\ActionInterface;
use Kitsunet\WebArchiver\Filters\FilterInterface;
use Symfony\Component\DomCrawler\Crawler as DOMCrawler;
use TYPO3\Flow\Utility\Files;

/**
 * Guzzle based Crawler.
 */
class Crawler {

	/**
	 * @var Client
	 */
	protected $guzzleClient;

	/**
	 * @var \Flowpack\SimpleSearch\Domain\Service\SqLiteIndex
	 */
	protected $currentSearchClient;

	/**
	 * @var Array
	 */
	protected $actions = array();

	/**
	 * @var Array
	 */
	protected $filters = array();

	/**
	 * @var callable
	 */
	protected $timeoutFunction;

	/**
	 * @var integer
	 */
	protected $maxDepth;

	/**
	 * Constructor.
	 *
	 * @param Client $client
	 */
	public function __construct($client) {
		$this->guzzleClient = $client;
		$this->timeoutFunction = function() { return 0;};
		$this->currentSearchClient = new \Flowpack\SimpleSearch\Domain\Service\SqLiteIndex('WebArchiver_' . md5($this->guzzleClient->getBaseUrl()));
	}

	/**
	 * This is the main action that will get called recursively depending on `$depth`.
	 *
	 *
	 * @return array
	 */
	public function go() {
		$result = $this->currentSearchClient->query('SELECT * FROM objects WHERE done = "0" ORDER BY depth ASC LIMIT 1');

		while (count($result) > 0) {
			$row = reset($result);
			$depth = $row['depth'];
			usleep($this->timeoutFunction->__invoke());
			$url = Url::factory($row['url']);
			$url = $url->makeAbsolute($this->getBaseUrl());
			$links = $this->processUrl($url);

			$this->currentSearchClient->indexData($url->getUniqueHash(), array(
				'done' => 1,
				'url' => $url->__toString(),
				'depth' => $depth
			), array());

			if ($this->getMaxDepth() === NULL || $this->getMaxDepth() >= $depth) {
				foreach ($links as $hash => $link) {
					$this->addUrl($link, $depth + 1);
				}
			}

			$result = $this->currentSearchClient->query('SELECT * FROM objects WHERE done = "0" ORDER BY depth ASC LIMIT 1');
		}

		return $this->currentSearchClient->query('SELECT * FROM objects WHERE done = "1" ORDER BY depth ASC');
	}

	/**
	 * Removes all entries from the crawl database for the current base url.
	 *
	 * @return void
	 */
	public function clearCrawlDatabase() {
		$this->currentSearchClient->flush();
	}

	/**
	 * @param integer $maxDepth
	 */
	public function setMaxDepth($maxDepth) {
		$this->maxDepth = $maxDepth;
	}

	/**
	 * @return integer
	 */
	public function getMaxDepth() {
		return $this->maxDepth;
	}

	/**
	 * @param string $requestUrl
	 * @param array $postData
	 * @throws \TYPO3\Flow\Utility\Exception
	 */
	public function doAuthenticationRequest($requestUrl, array $postData) {
		$cookiePath = FLOW_PATH_DATA . 'Temporary/WebArchiver/';
		$cookieName = md5($this->guzzleClient->getBaseUrl());
		Files::createDirectoryRecursively($cookiePath);
		file_put_contents($cookiePath . $cookieName, json_encode(array()));

		$cookiePlugin = new CookiePlugin(new FileCookieJar($cookiePath . $cookieName));
		$this->guzzleClient->addSubscriber($cookiePlugin);
		$authenticationRequest = $this->guzzleClient->post($requestUrl, NULL, $postData);
		$authenticationRequest->send();
	}

	/**
	 * @param string $formUrl
	 * @param string $formXPath
	 * @param array $formData
	 * @throws \TYPO3\Flow\Utility\Exception
	 */
	public function sendLoginForm($formUrl, $formXPath, array $formData) {
		$cookiePath = FLOW_PATH_DATA . 'Temporary/WebArchiver/';
		$cookieName = md5($this->guzzleClient->getBaseUrl());
		Files::createDirectoryRecursively($cookiePath);
		file_put_contents($cookiePath . $cookieName, json_encode(array()));

		$cookiePlugin = new CookiePlugin(new FileCookieJar($cookiePath . $cookieName));
		$this->guzzleClient->addSubscriber($cookiePlugin);

		$authenticationFormRequest = $this->guzzleClient->get($formUrl);
		$authenticationFormResponse = $authenticationFormRequest->send();
		$domCrawler = new DOMCrawler($authenticationFormResponse->getBody(TRUE), $formUrl);
		$xPathResult = $domCrawler->filterXPath($formXPath);
		$form = $xPathResult->form($formData);

		$loginRequest = $this->guzzleClient->post($form->getUri(), null, $form->getPhpValues());
		$loginRequest->send();
	}

	/**
	 * @param Url $url
	 * @return array
	 */
	protected function processUrl($url) {
		$response = NULL;
		$links = array();
		try {
			$request = $this->guzzleClient->get($url);
			$response = $request->send();
			$crawledUrl = new CrawledUrlContext($url, $request, $response);
		} catch (\Exception $e) {
			return $links;
		}

		if ($response === NULL) {
			return $links;
		}
		$this->processActions($crawledUrl);

		if (strpos($response->getContentType(), 'html') !== FALSE) {
			$links = $this->filterUrls($this->findAllLinks($url, $response->getBody()));
		}


		return $links;
	}

	/**
	 * @param array $urls
	 * @return array
	 */
	public function filterUrls(array $urls) {
		$filteredUrls = array();
		foreach ($urls as $hash => $url) {
			if ($this->urlPassesFilters($url)) {
				$filteredUrls[$hash] = $url;
			}
		}
		return $filteredUrls;
	}

	/**
	 * @param Url $url
	 * @param integer $depth
	 */
	public function addUrl(Url $url, $depth = 0) {
		if ($this->urlPassesFilters($url)) {
			$result = $this->currentSearchClient->query('SELECT * FROM objects WHERE __identifier__ LIKE "' . $url->getUniqueHash() . '";');
			if (count($result) === 0) {
				$this->currentSearchClient->indexData($url->getUniqueHash(), array(
					'url' => $url->__toString(),
					'done' => 0,
					'depth' => $depth
				), array());
			}
		}
	}

	/**
	 * Runs all filters against the url.
	 *
	 * @param Url $url
	 *
	 * @return boolean
	 */
	public function urlPassesFilters($url) {
		foreach ($this->filters as $filter) {
			if (!$filter->filter($url)) {
				return FALSE;
			}
		}
		return TRUE;
	}

	/**
	 * Process the result by execution all actions.
	 *
	 * @param CrawledUrlContext $crawledUrl
	 */
	public function processActions($crawledUrl) {
		foreach ($this->actions as $action) {
			$result = $action->execute($crawledUrl);
		}
		unset($result);
	}

	/**
	 * @return \Guzzle\Http\Url|string
	 */
	public function getBaseUrl() {
		return $this->guzzleClient->getBaseUrl();
	}

	/**
	 * Add actions that will be executed on the results.
	 *
	 * @param ActionInterface $action
	 */
	public function addAction(ActionInterface $action) {
		$action->setCrawler($this);
		$this->actions[] = $action;
	}

	/**
	 * Add another filter.
	 *
	 * @param FilterInterface $filter
	 */
	public function addFilter(FilterInterface $filter) {
		$this->filters[] = $filter;
	}

	/**
	 * Finds all links from a HTML document.
	 *
	 * @param Url $parentUrl
	 * @param string $html
	 *
	 * @return array
	 */
	public function findAllLinks($parentUrl, $html) {
		$domCrawler = new DOMCrawler();
		$domCrawler->addContent($html);

		$xpathLinksWithUrl = '//a[@href]';
		$links = $domCrawler->filterXPath($xpathLinksWithUrl)->each(
			function ($node, $i) use ($parentUrl) {
				/**
				 * @var $node \DOMElement
				 */
				$url = $node->getAttribute('href');

				$urlObject = Http\Url::factory($url);
				$urlObject->setFragment(NULL);

				if ($urlObject->__toString() === '') {
					return array();
				}

				$absoluteUrl = $urlObject->makeAbsolute($this->getBaseUrl(), $parentUrl);
				return array(
					'hash' => $absoluteUrl->getUniqueHash(),
					'target' => $absoluteUrl
				);
			}
		);

		$cleanLinks = array();
		foreach ($links as $link) {
			if ($link !== array()) {
				$cleanLinks[$link['hash']] = $link['target'];
			}
		}

		return $cleanLinks;
	}

	/**
	 * @param integer $timeoutMin Milliseconds wait until next url is crawled
	 * @param integer $timeoutMax if set a random interval between min and max is waited
	 */
	public function setTimeout($timeoutMin, $timeoutMax = NULL) {
		if ($timeoutMin === 0) {
			$this->timeoutFunction = function() {
				return 0;
			};
		}
		if ($timeoutMax !== NULL && $timeoutMax > $timeoutMin) {
			$this->timeoutFunction = function() use ($timeoutMin, $timeoutMax) {
				return rand($timeoutMin, $timeoutMax) * 1000;
			};
		} else {
			$this->timeoutFunction = function () use ($timeoutMin, $timeoutMax) {
				return $timeoutMin * 1000;
			};
		}
	}

}