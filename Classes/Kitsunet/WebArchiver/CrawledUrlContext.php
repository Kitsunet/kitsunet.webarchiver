<?php
namespace Kitsunet\WebArchiver;

use Guzzle\Http\Message\RequestInterface;
use Guzzle\Http\Message\Response;
use Kitsunet\WebArchiver\Http\Url;

/**
 * Class CrawledUrlContext
 *
 */
class CrawledUrlContext {

	/**
	 * @var Url
	 */
	protected $url;

	/**
	 * @var RequestInterface
	 */
	protected $request;

	/**
	 * @var Response
	 */
	protected $response;

	/**
	 * Constructor.
	 *
	 * @param Url $url
	 * @param RequestInterface $request
	 * @param Response $response
	 */
	public function __construct($url, $request, $response) {
		$this->url = $url;
		$this->request = $request;
		$this->response = $response;
	}

	/**
	 * @return RequestInterface
	 */
	public function getRequest() {
		return $this->request;
	}

	/**
	 * @return Response
	 */
	public function getResponse() {
		return $this->response;
	}

	/**
	 * @return Url
	 */
	public function getUrl() {
		return $this->url;
	}
}
