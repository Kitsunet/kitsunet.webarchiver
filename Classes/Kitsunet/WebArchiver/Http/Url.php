<?php
namespace Kitsunet\WebArchiver\Http;

/**
 * Parses and generates URLs based on URL parts. In favor of performance, URL parts are not validated.
 */
class Url extends \Guzzle\Http\Url {

	/**
	 * @param boolean $stripFragment
	 * @return string
	 */
	public function getUniqueHash($stripFragment = TRUE) {
		if ($stripFragment === TRUE) {
			$fragment = $this->getFragment();
			$this->setFragment(NULL);
			$uniqueHash = md5(json_encode($this->getParts()));
			$this->setFragment($fragment);
		} else {
			$uniqueHash = md5(json_encode($this->getParts()));
		}

		return $uniqueHash;
	}

	/**
	 * @param string $domain
	 * @param Url $parentUrl
	 * @return Url
	 */
	public function makeAbsolute($domain, Url $parentUrl = NULL) {
		if ($this->getHost() === NULL && $this->getScheme() === NULL) {
			$urlPath = $this->getPath();
			if (substr($urlPath, 0, 1) === '/') {
				$baseUrl = static::factory($domain);
				$this->setHost($baseUrl->getHost());
				$this->setScheme($baseUrl->getScheme());
			} else {
				if ($parentUrl === NULL) {
					throw new \InvalidArgumentException('To convert a relative url (' . $this->__toString() . ') to absolute, a parentUrl has to be given. None given.', 1408123873);
				}
				$absoluteUrlPath = dirname($parentUrl->getPath()) . '/' . $urlPath;
				return $this->makeUrlAbsolute($this->setPath($absoluteUrlPath));
			}
		}

		return $this;
	}
}
