<?php
namespace Kitsunet\WebArchiver\Actions;

use Kitsunet\WebArchiver\Crawler;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Mirror Result Action
 *
 */
class MirrorResultAction implements ActionInterface {
	/**
	 * @var $directoryBase
	 */
	protected $directoryBase;

	/**
	 * @var Crawler
	 */
	protected $crawler;

	/**
	 * Constructor.
	 *
	 * @param String $directoryBase
	 */
	public function __construct($directoryBase) {
		if (!is_dir($directoryBase) || !is_writable($directoryBase)) {
			throw new \InvalidArgumentException("$directoryBase is not a directory or not writable", 1);
		}
		$this->directoryBase = realpath($directoryBase) . '/';
	}

	/**
	 * @param Crawler $crawler
	 * @return mixed
	 */
	public function setCrawler(Crawler $crawler) {
		$this->crawler = $crawler;
		$baseUrl = $crawler->getBaseUrl();
		if (!$baseUrl instanceof \Guzzle\Http\Url) {
			$baseUrl = \Kitsunet\WebArchiver\Http\Url::factory($baseUrl);
		}
		$this->directoryBase .= str_replace('.', '_', $baseUrl->getHost()) . '/';
	}


	/**
	 * Execute an action on the result.
	 *
	 * @param \Kitsunet\WebArchiver\CrawledUrlContext $crawledUrl
	 * @return \Kitsunet\WebArchiver\CrawledUrlContext
	 */
	public function execute(\Kitsunet\WebArchiver\CrawledUrlContext $crawledUrl) {
		$fs = new Filesystem();
		if (strpos($crawledUrl->getResponse()->getContentType(), 'html') === FALSE) {
			$filePath = $this->directoryBase . $crawledUrl->getUrl()->getPath();
			$fs->dumpFile($filePath, $crawledUrl->getResponse()->getBody());
			return $crawledUrl;
		}
		$urlObject = $crawledUrl->getUrl();
		$url = $urlObject->getPath();
		if ($url === '' || $url === '/') {
			$url .= 'index';
		}

		$dom = $this->createDomDocument($crawledUrl->getResponse()->getBody());
		$this->removeBaseHref($dom);
		$this->extractStyles($dom, $urlObject);
		$this->extractScripts($dom, $urlObject);
		$this->extractImages($dom, $urlObject);

		// URLs like blog.com/posts and blog.com/posts/a would conflict, because
		// the first URL would be created as a file and the same name cannot be used for
		// a directory with the same name. Workaround is to simply attach an underscore
		// to the *index* file.
		if ($this->isPrettyUrl($url)) {
			$url = $url . '.html';
		}

		$saveTo = $this->directoryBase . DIRECTORY_SEPARATOR . $url;

		$fs->dumpFile($saveTo, $dom->saveHTML());

		return $crawledUrl;
	}

	protected function createDomDocument($content, $type = NULL) {
		libxml_use_internal_errors(TRUE);
		if (empty($type)) {
			$type = 0 === strpos($content, '<?xml') ? 'application/xml' : 'text/html';
		}

		$charset = NULL;
		if (FALSE !== $pos = stripos($type, 'charset=')) {
			$charset = substr($type, $pos + 8);
			if (FALSE !== $pos = strpos($charset, ';')) {
				$charset = substr($charset, 0, $pos);
			}
		}

		// http://www.w3.org/TR/encoding/#encodings
		// http://www.w3.org/TR/REC-xml/#NT-EncName
		if (NULL === $charset &&
			preg_match('/\<meta[^\>]+charset *= *["\']?([a-zA-Z\-0-9_:.]+)/i', $content, $matches)
		) {
			$charset = $matches[1];
		}

		if (NULL === $charset) {
			$charset = 'UTF-8';
		}

		$dom = new \DOMDocument('1.0', $charset);
		$dom->loadHTML($content);
		return $dom;
	}

	protected function removeBaseHref($dom) {
		foreach ($dom->getElementsByTagName('base') as $baseTag) {
			$baseTag->parentNode->removeChild($baseTag);
		}
	}

	protected function extractStyles($dom, $url) {
		foreach ($dom->getElementsByTagName('link') as $linkTag) {
			$fs = new Filesystem();
			$href = $linkTag->getAttribute('href');

			// this url has already been parsed
			if ($href === '' || $href === '#') {
				continue;
			}

			$hrefUrlObject = \Kitsunet\WebArchiver\Http\Url::factory($href);
			$hrefUrlObject->makeAbsolute($this->crawler->getBaseUrl(), $url);

			if ($this->crawler->urlPassesFilters($hrefUrlObject)) {
				$this->crawler->addUrl($hrefUrlObject);
				$linkTag->setAttribute('href', $hrefUrlObject->getPath());
			}
		}
	}

	protected function extractScripts($dom, $url) {
		foreach ($dom->getElementsByTagName('script') as $linkTag) {
			$fs = new Filesystem();
			$src = $linkTag->getAttribute('src');
			if ($src === '' || $src === '#') {
				continue;
			}

			$hrefUrlObject = \Kitsunet\WebArchiver\Http\Url::factory($src);
			$hrefUrlObject->makeAbsolute($this->crawler->getBaseUrl(), $url);

			if ($this->crawler->urlPassesFilters($hrefUrlObject)) {
				$this->crawler->addUrl($hrefUrlObject);
				$linkTag->setAttribute('src', $hrefUrlObject->getPath());
			}
		}
	}

	protected function extractImages($dom, $url) {
		foreach ($dom->getElementsByTagName('img') as $linkTag) {
			$fs = new Filesystem();
			$src = $linkTag->getAttribute('src');
			if ($src === '' || $src === '#') {
				continue;
			}

			$hrefUrlObject = \Kitsunet\WebArchiver\Http\Url::factory($src);
			$hrefUrlObject->makeAbsolute($this->crawler->getBaseUrl(), $url);

			if ($this->crawler->urlPassesFilters($hrefUrlObject)) {
				$this->crawler->addUrl($hrefUrlObject);
				$linkTag->setAttribute('src', $hrefUrlObject->getPath());
			}
		}
	}

	/**
	 * Checks is a URL is pretty.
	 *
	 * Pretty URLs contain no file endings like `.php`. The check is not
	 * 100%, but checking for the usual file ending patterns should do.
	 * Plain domain names are always pretty.
	 *
	 * @param  String $url
	 *
	 * @return bool
	 */
	public function isPrettyUrl($url) {
//        $regex = '/
//            .*\/            # get as many characters up to the last /
//            [^\.]+          # move forward till the last .
//            \.\w{2,4}$      # check if there is a file ending at the end
//            /x';
		$regex = '/.*\/[^\.]+\.\w{2,4}$/';
		return 1 !== preg_match($regex, $url);
	}
}
