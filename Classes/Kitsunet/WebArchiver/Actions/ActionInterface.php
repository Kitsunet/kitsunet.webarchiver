<?php
namespace Kitsunet\WebArchiver\Actions;

use Kitsunet\WebArchiver\Crawler;

/**
 * Interface ActionInterface
 *
 */
interface ActionInterface {

	/**
	 * Execute an action on the result.
	 *
	 * @param  \Kitsunet\WebArchiver\CrawledUrlContext $result
	 *
	 * @return \Kitsunet\WebArchiver\CrawledUrlContext
	 */
	public function execute(\Kitsunet\WebArchiver\CrawledUrlContext $crawledUrl);

	/**
	 * @param Crawler $crawler
	 * @return mixed
	 */
	public function setCrawler(Crawler $crawler);
}